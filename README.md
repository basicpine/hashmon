# HashMon
```
 __________
/\____;;___\      A script to monitor for new responder hashes and auto
| /         /     submit them to a Hashtopolis server.
 `. -------- .
  |\ Hash-Mon\
  | |---------|   Example: python ./HashMon.py http(s)://<IP Of Hashtopolis> <Client Name>
  \ |    ))   |
   \|_________|   Script will auto look for a hashtopolis api key in a env variable
                  called HASHTOPOLIS, or user can enter it in after running the script.
           
                  SUPPORTED HASHES: NTLMv1, NTLMv2
```
Currently in beta...

### Things to do
- [x] Add Error Checking
- [ ] Add More Error Checking
- [ ] Add support for multiple hashes
- [ ] Auto start cracking tasks
- [ ] Complile hash lists into a superhashlist
- [ ] Display cracked users and passwords
- [ ] Add cracked passwords to cme database
- [ ] Monitor mitm6 hashes
- [ ] Save hashes for easy resume
